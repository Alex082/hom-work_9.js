/*

1 Опишіть, як можна створити новий HTML тег на сторінці.

  document.createElement - и передаём 2 параметра имя имя тега и второй не обязательный параметр.

2 Опишіть, що означає перший параметр функції 
  insertAdjacentHTML і опишіть можливі варіанти цього параметра.

  он указивает на то где нужно вставить HTML код, оносительно того к чему мы его добавляем,
  этот параметр будет рядком и: beforebegin,
                                afterbegin,
                                beforeend,
                                afterend

3 Як можна видалити елемент зі сторінки?

  нужно выбрать этот элемент и к нему через точку прописать remove()

*/

// создаём функцыю
function runList(items, parent = document.body) {
  // создаём список и добавляем его
  let list = document.createElement("ul");
  parent.appendChild(list);

  // идём по списку и создаём елемент li и добавляем его
  items.forEach((item) => {
    let listItem = document.createElement("li");
    list.appendChild(listItem);

    // делаем проверку на массив в массиве тогда вызываем рекурсию,
    // если нет тогда добавляем его к li
    if (Array.isArray(item)) {
      runList(item, listItem);
    } else {
      let text = document.createTextNode(item);
      listItem.appendChild(text);
    }
  });
}

const items1 = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
runList(items1);

const items2 = ["1", "2", "3", "sea", "user", 23];
const parent = document.getElementById("my-list");
runList(items2, parent);

const items3 = [
  "Kharkiv",
  "Kiev",
  ["Borispol", "Irpin"],
  "Odessa",
  "Lviv",
  "Dnieper",
];
runList(items3);
